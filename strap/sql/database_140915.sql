-- MySQL dump 10.15  Distrib 10.0.21-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: holbiadmin_stats
-- ------------------------------------------------------
-- Server version	10.0.21-MariaDB-1~wheezy-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `backup_status`
--

DROP TABLE IF EXISTS `backup_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backup_status` (
  `hostname` varchar(255) NOT NULL,
  `backdate` varchar(32) NOT NULL,
  PRIMARY KEY (`hostname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `backup_status`
--

LOCK TABLES `backup_status` WRITE;
/*!40000 ALTER TABLE `backup_status` DISABLE KEYS */;
INSERT INTO `backup_status` VALUES ('d11uk.holbihost.co.uk','2015-09-14 02:56:35'),('ms1.holbihost.co.uk','2015-09-14 02:15:35'),('ms7.holbihost.co.uk','2015-09-14 01:04:48'),('ms8.holbihost.co.uk','2015-09-14 01:23:57'),('serveremo1.emanualonline.com','2015-09-14 09:02:21');
/*!40000 ALTER TABLE `backup_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `checksum_status`
--

DROP TABLE IF EXISTS `checksum_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `checksum_status` (
  `id` int(11) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `checkdate` varchar(32) NOT NULL,
  `status` varchar(32) NOT NULL,
  `version` varchar(32) NOT NULL,
  `report` text NOT NULL,
  UNIQUE KEY `hostname` (`hostname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `checksum_status`
--

LOCK TABLES `checksum_status` WRITE;
/*!40000 ALTER TABLE `checksum_status` DISABLE KEYS */;
INSERT INTO `checksum_status` VALUES (0,'cuddlykingdom.com','2015-08-13 16:52:16','ok','0.4.150417',''),(0,'dcp-shop.co.uk','2015-09-14 00:01:09','check','0.4.150417',''),(0,'dodsshop.co.uk','2015-09-14 16:25:14','ok','0.4.150417',''),(0,'elfadventures.co.uk','2015-08-13 16:58:08','ok','0.4.150417',''),(0,'folkshop.efdss.org','2015-06-22 11:29:53','ok','0.4.150417',''),(0,'funkycondom.com','2015-06-03 09:35:25','ok','0.4.150417',''),(0,'funkypepper.co.uk','2015-08-13 14:51:21','ok','0.4.150417',''),(0,'gbracing.eu','2015-09-14 13:03:20','ok','0.4.150417',''),(0,'i-sunglasses.com','2015-06-29 15:23:50','ok','0.4.150417',''),(0,'iomrp.im','2015-07-07 11:15:34','ok','0.4.150417',''),(0,'kinetiklamps.co.uk','2015-09-07 14:08:04','ok','0.4.150417',''),(0,'lumalighting.co.uk','2015-09-14 16:20:41','ok','0.4.150417',''),(0,'no1lites.com','2015-04-22 17:45:54','ok','0.4.150417',''),(0,'packagingandmuchmore.co.uk','2015-05-13 11:35:20','ok','0.4.150417',''),(0,'pamper-me-store.co.uk','2015-09-14 16:15:44','ok','0.4.150417',''),(0,'phdesigns.co.uk','2015-09-14 12:49:28','ok','0.4.150417',''),(0,'portableuniverse.co.uk','2015-09-07 14:06:20','ok','0.4.150818',''),(0,'rings-direct.co.uk','2015-09-14 16:34:51','ok','0.4.150417',''),(0,'simpsonfineart.com','2015-09-14 15:43:43','ok','0.4.150417',''),(0,'tattooforaweek.com','2015-09-14 12:45:05','ok','0.4.150417',''),(0,'thehairshop.co.uk','2015-08-11 10:55:10','ok','0.4.150417',''),(0,'tool-shop.co.uk','2015-08-11 10:53:12','ok','0.4.150417','');
/*!40000 ALTER TABLE `checksum_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mirror_status`
--

DROP TABLE IF EXISTS `mirror_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mirror_status` (
  `hostname` varchar(255) NOT NULL,
  `backdate` varchar(32) NOT NULL,
  PRIMARY KEY (`hostname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mirror_status`
--

LOCK TABLES `mirror_status` WRITE;
/*!40000 ALTER TABLE `mirror_status` DISABLE KEYS */;
INSERT INTO `mirror_status` VALUES ('ms5.holbihost.co.uk','2015-09-14 09:30:25'),('server.womenssuite.com','2015-09-14 02:01:24'),('www.almostgone.co.uk','2015-09-14 01:02:21');
/*!40000 ALTER TABLE `mirror_status` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-14 18:00:12
